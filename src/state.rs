use crate::models::language::Language;
use crate::models::settings::Settings;

pub struct State {
    pub selected_source_language_code: String,
    pub selected_target_language_code: String,
    pub source_languages: Vec<Language>,
    pub target_languages: Vec<Language>,
    pub source_text: String,
    pub target_text: String,
    pub settings: Settings,
}

impl State {
    pub fn new(settings: Settings, source_languages: Vec<Language>, target_languages: Vec<Language>) -> Self {
        Self {
            selected_source_language_code: settings.default_source_language_code.clone()
                .expect("No default source language defined."),
            selected_target_language_code: settings.default_target_language_code.clone()
                .expect("No default target language defined."),
            source_languages,
            target_languages,
            source_text: String::new(),
            target_text: String::new(),
            settings,
        }
    }
}
