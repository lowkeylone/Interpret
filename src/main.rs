extern crate gtk;
extern crate glib;
extern crate libhandy;

#[macro_use]
extern crate derive_more;

use std;
use std::rc::Rc;

mod models;
mod services;
mod state;
mod ui;

use ui::main_window::MainWindow;
use models::language::Language;
use models::settings::Settings;
use state::State;
use std::cell::RefCell;
use crate::services::deepl_service::DeeplService;

fn main() {
    gtk::init().expect("Failed to initialize GTK3.");
    libhandy::init();

    let settings = Settings::load_default_settings();
    let source_languages = Language::load_source_languages();
    let target_languages = Language::load_target_languages();
    let deepl_service = Rc::new(DeeplService::new(settings.api_key.clone()));
    let state = Rc::new(RefCell::new(State::new(settings, source_languages, target_languages)));
    let gui = Rc::new(MainWindow::new(&state.borrow()));

    {
        let _gui = Rc::clone(&gui);
        let deepl_service = Rc::clone(&deepl_service);
        let state = Rc::clone(&state);

        &gui.on_translate_button_click(move |_| {
            let mut state = state.borrow_mut();

            let translation = deepl_service.translate(
                &state.source_text,
                &state.selected_source_language_code,
                &state.selected_target_language_code
            );

            match translation {
                Ok(translation) => {
                    translation.map(|t| state.target_text = t.text)
                },
                _err => {None}
            };

            _gui.update_from(&state);
            _gui.switch_to_result_page();
        });
    }

    {
        let _gui = Rc::clone(&gui);

        &gui.on_back_button_click(move |_| {
            _gui.switch_to_translation_page();
        });
    }

    {
        let _gui = Rc::clone(&gui);
        let state = Rc::clone(&state);

        &gui.on_source_language_button_click(Rc::new(move |language_code: &str| {
            let mut state = state.borrow_mut();
            state.selected_source_language_code = language_code.to_string();
            _gui.update_from(&state);
        }));
    }

    {
        let _gui = Rc::clone(&gui);
        let state = Rc::clone(&state);

        &gui.on_target_language_button_click(Rc::new(move |language_code: &str| {
            let mut state = state.borrow_mut();
            state.selected_target_language_code = language_code.to_string();
            _gui.update_from(&state);
        }));
    }

    {
        let _gui = Rc::clone(&gui);
        let state = Rc::clone(&state);

        &gui.on_source_text_buffer_changed(move |_| {
            let mut state = state.borrow_mut();
            state.source_text = _gui.get_source_text();
            _gui.update_from(&state);
        });
    }

    gui.start();
    gtk::main();
}
