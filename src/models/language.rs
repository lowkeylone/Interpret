use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Language {
    pub code: String,
    pub name: String,
}

impl Language {

    pub fn load_source_languages() -> Vec<Language> {
        let languages_str = include_str!("../../resources/source_languages.json");
        Language::load_languages(languages_str)
    }

    pub fn load_target_languages() -> Vec<Language> {
        let languages_str = include_str!("../../resources/target_languages.json");
        Language::load_languages(languages_str)
    }

    fn load_languages(languages_str: &str) -> Vec<Language> {
        let languages: Result<Vec<Language>, serde_json::Error> = serde_json::from_str(languages_str);

        match languages {
            Ok(mut languages) => {
                languages.sort_by_key(|language| language.name.clone());
                languages
            },
            Err(_) => panic!("Unable to load languages.")
        }
    }

}
