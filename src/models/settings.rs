use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Settings {
    pub api_key: Option<String>,
    pub built_in_api_key: Option<String>,
    pub default_source_language_code: Option<String>,
    pub default_target_language_code: Option<String>
}

impl Settings {

    pub fn load_default_settings() -> Settings {
        let default_settings_str = include_str!("../../resources/default-settings.json");
        let settings: Result<Settings, serde_json::Error> = serde_json::from_str(default_settings_str);

        match settings {
            Ok(settings) => {
                Settings {
                    api_key: settings.built_in_api_key.clone(),
                    default_source_language_code: settings.default_source_language_code.or(Some("FR".to_string())).clone(),
                    default_target_language_code: settings.default_target_language_code.or(Some("EN-US".to_string())).clone(),
                    ..settings
                }
            },
            Err(_) => panic!("Unable to load default settings.")
        }
    }

}
