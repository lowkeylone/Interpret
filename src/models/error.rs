#[derive(From)]
pub enum Error {
    HttpError(isahc::Error),
    ParsingError(serde_json::Error),
    IoError(std::io::Error),
    NoApiKey
}
